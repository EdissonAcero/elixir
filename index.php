<?php

?>

<!doctype html>
<html lang="es">
    <!--<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="generator" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
        
        <title>TuTrago</title>
        
        <link rel="icon" type="image/png" href="img/logoli.png">
    </head> -->
     <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <link href="css/style.css" rel="stylesheet">
        
        <link href="https://fonts.googleapis.com/css?family=Krub:400,700" rel="stylesheet">
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
        <link href="https://bootswatch.com/4/lux/bootstrap.css" rel="stylesheet" />	
        
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        
        <title>TuTrago</title>
        
        <link rel="icon" type="image/png" href="img/logoli.png">
       
    </head> 
    <body>
    
    	<?php
           include "presentacion/vistas/vistaInicio.php";
        ?>  
    
    </body>
</html>

